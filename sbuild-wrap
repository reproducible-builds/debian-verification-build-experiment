#!/bin/sh

set -e
buildinfo=$1
buildinfo_sources_list=${buildinfo}.snapshots.list
testdir=/home/vagrant/tests
parallel=7
tail_build_dir=$(echo $buildinfo | sed -e 's,.*srv/ftp-master.debian.org/buildinfo/,,g' -e 's,-buildd.buildinfo,,g')
build_dir=$testdir/$tail_build_dir
mkdir -vp $build_dir
cd $build_dir

buildinfo_stripped="$(basename $buildinfo)"
gpg -d "$buildinfo" 2> /dev/null > $buildinfo_stripped || true
source="$(grep-dctrl --no-field-names --show-field Source . $buildinfo_stripped | awk '{print $1}')"
version="$(grep-dctrl --no-field-names --show-field version . $buildinfo_stripped)"
epoch=""
case ${version} in
       *:*) epoch="$(echo $version | cut -d : -f 1):"
	    version="$(echo $version | cut -d : -f 2)"
	    ;;
esac
# strip out binNMU
source_version="$(echo $version | sed -e 's,\+b[0-9]$,,g' -e 's,\+b[0-9][0-9]$,,g')"
binnmu_changelog="$(grep-dctrl --show-field Binary-Only-Changes . $buildinfo_stripped | sed -e 's,^ ,,g' -e 's,^\.,,g' -e '/Binary-Only-Changes/d')"
arch="$(grep-dctrl --no-field-names --show-field Architecture . $buildinfo_stripped)"
depends="$(grep-dctrl --no-field-names --show-field Installed-Build-Depends . $buildinfo_stripped)"
buildpath="$(grep-dctrl --no-field-names --show-field Build-Path: . $buildinfo_stripped)"
debchecksums="$(grep-dctrl --no-field-names --show-field Checksums-Sha256 . $buildinfo_stripped)"
packages_checksum="$(grep-dctrl --no-field-names --show-field Checksums-Sha256 . $buildinfo_stripped | sha256sum)"
local_buildinfo=$(basename $buildinfo | sed -e 's,-buildd,,'g)

if [ -s "$local_buildinfo" ]; then
    echo "Skipping build of $buildinfo, $local_buildinfo already present"
    exit 1
fi

# build always uses amd64, even when only building arch:all
buildlog=${source}_${version}_amd64.build
if [ -L "$buildlog" ]; then
    echo "Skipping build of $buildinfo, already attempted: $buildlog"
    exit 1
fi

case ${buildpath} in
    # Needs to *usually* be /build/reproducible-path/ but sbuild appends
    # PACKAGE-VERSION, so we need to strip it out to run with sbuild again.
    # Buildd.debian.org uses this since 2023-07-19
    # https://bugs.debian.org/1034424
    /build/reproducible-path*) buildpath=/build/reproducible-path ;;
esac
buildpath=$(echo ${buildpath} | sed -e "s,/${source}-${version},,g")

case ${version} in
    *deb11u*|*deb10u*|*bpo11*|*bpo10*) echo "not going to build $buildinfo for bullseye or buster, sorry, exiting..."
		       exit 1 ;;
esac

archarg="--no-arch-all --arch-any"
case ${arch} in
    all) archarg="--arch-all --no-arch-any" ;;
esac

# default to trixie for most builds, as it is usually easier to
# upgrade than downgrade if needed.
base_dist=trixie
case ${source}_${version} in
    *_*deb12u*|*_*bpo*|linux*_6.1.*) base_dist=bookworm ;;
esac

standard_sbuild_options="--chroot-mode=unshare
	-c ${base_dist}-amd64
	-d UNRELEASED
	--no-source
	--no-run-lintian
	--no-apt-upgrade
	--no-apt-distupgrade
       --bd-uninstallable-explainer=apt
       --build-dep-resolver=aptitude"

# FIXME handling anything with whitespace (--extra-repository,
# --binNMU-changelog, --add-depends), duplicating lots of entries for
# no real good reason.
#
# Yes, include all the repositories since bookworm, in case we have
# some weird corner-case.
if [ -z "$binnmu_changelog" ]; then
    LANG=C.UTF-8 DEB_BUILD_OPTIONS=parallel=${parallel} sbuild ${standard_sbuild_options} \
       --chroot-setup-commands='rm -vf /etc/apt/sources.list' \
       --chroot-setup-commands='sed -i -e "/acquire::http::proxy/d" /etc/apt/apt.conf.d/99mmdebstrap' \
       --extra-repository="$(cat ${buildinfo_sources_list})" \
       --build-path="${buildpath}" \
       ${archarg} \
       --add-depends="$depends" \
       ${source}_${source_version} || touch "${source}_${version}_${arch}.failed"
else
    LANG=C.UTF-8 DEB_BUILD_OPTIONS=parallel=${parallel} sbuild ${standard_sbuild_options} \
       --binNMU-changelog="${binnmu_changelog}" \
       --chroot-setup-commands='rm -vf /etc/apt/sources.list' \
       --chroot-setup-commands='sed -i -e "/acquire::http::proxy/d" /etc/apt/apt.conf.d/99mmdebstrap' \
       --extra-repository="$(cat ${buildinfo_sources_list})" \
       --build-path="${buildpath}" \
       ${archarg} \
       --add-depends="$depends" \
       ${source}_${source_version} || touch "${source}_${version}_${arch}.failed"
fi

if [ -s "${local_buildinfo}" ]; then
    local_packages_checksum="$(grep-dctrl --no-field-names --show-field Checksums-Sha256 . $local_buildinfo | sha256sum)"
    if [ "$local_packages_checksum" = "$packages_checksum" ]; then
	reproducible_file="${source}_${version}_${arch}.reproducible"
	echo REPRODUCIBLE $local_buildinfo $buildinfo
	grep-dctrl --no-field-names --show-field Checksums-Sha256 . $local_buildinfo | tee ${reproducible_file}
	rm $buildinfo_stripped
    else
	unreproducible_file="${source}_${version}_${arch}.unreproducible"
	echo NOTREPRODUCIBLE $local_buildinfo $buildinfo
	diff -u $buildinfo_stripped $local_buildinfo | tee ${unreproducible_file}
    fi
else
    echo "local buildinfo $local_buildinfo not found, build must have failed"
    touch "${source}_${version}_${arch}.missingbuildinfo"
    exit 1
fi
